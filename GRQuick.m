(* ::Package:: *)

(* Package: GRQuick *)
(* Description: Mathematica package inspired/based by GRQUICK by Jaime
   Varela *)
(* Version: 0.0.1 *)
(* Author: Maciej Maliborski, maciej.maliborski@aei.mpg.de *)


BeginPackage["GRQuick`"];


GRQSimplify::usage = "GRQSimplify[] is used internally by GRQ to\
 simplify calculations. By default it is set to Identity.";

GRQCreateMetric::usage = "\!\(GRQCreateMetric[\*StyleBox[g,\"TI\"],\*StyleBox[c,\"TI\"]] constructs a metric with components g and list of coordinates \*StyleBox[c, \"TI\"].\)
\!\(GRQCreateMetric[\*StyleBox[g,\"TI\"],\*StyleBox[c,\"TI\"]] returns GRQMetric object.\)";

GRQMetric::usage = "\!\(GRQMetric[\*StyleBox[data,\"TI\"]] an object representing metric created by GRQCreateMetric.\)";

GRQChristoffel::usage = "GRQChristoffel[g][{{i},{k,l}}] returns\
 (i,k,l)-th component of the Christoffel symbol for the metric g.";

GRQRiemann::usage = "GRQRiemann[g][{{i},{j,k,l}}] returns (i,j,k,l)-th\
 component of the Riemann tensor for the metric g."

GRQRicci::usage = "GRQRicci[g][{{},{i,j}}] returns (i,j)-th component\
 of the Ricci tensor for the metric g.";

GRQRicciScalar::usage = "GRQRicciScalar[g][] returns Ricci scalar for\
 the metric g.";

GRQEinstein::usage = "GRQEinstein[g][{{},{i,j}}] returns (i,j)-th \
component of the Einstein tensor for the metric g.";

GRQCoordsToOneForms::usage = "GRQCoordsToOneForms[c] returns list of\
 symbols representing one forms of c.
GRQCoordsToOneForms[c, d] returns list of symbols with prefix d.";

GRQLineElementToMatrix::usage = "GRQLineElementToMatrix[ds, dc] returns\
 metric matrix corresponding to line element ds knowing the one forms.";

GRQClearMetricCache::usage = "GRQClearMetricCache[g] clear saved definitions\
 associated with metric g.";

GRQMetric3p1Form::usage = "GRQMetric3p1Form[g] returns the components of\
 metric g in 3+1 decomposition.";

GRQExtrinsicCurvature::usage = "GRQExtrinsicCurvature[g] returns the\
 components of extrinsic curvature.";

GRQSchouten::usage = "GRQSchouten[g][{{}, {i,j}}] returns (i,j)-th \
component of the Schouten tensor for the metric g.";

GRQCotton::usage = "GRQCotton[g][{{}, {i,j,k}}] returns (i,j,k)-th \
component of the Cotton tensor for the metric g.";

GRQWeyl::usage = "GRQWeyl[g][{{a}, {b,c,d}}] returns (a,b,c,d)-th \
component of the Weyl tensor for the metric g.";



Begin["`Private`"];


(* Internal tools *)
ListOfSymbolsQ[l_List] := And @@ (Or[Head[#] === Symbol, Head[#] === Subscript] & /@ l);

DuplicatesQ[l_List] := Not[DeleteDuplicates[l] == l];

GRQSimplify = Identity;

inRangeQ[a__, {min_, max_}] := And @@ Map[min <= # <= max &, Flatten @ {a}];


(* GRQProduct::wlen = "Vectors of incompatible lengths (`1`, `2`) passed."; *)
(* GRQProduct::warg = "Wrong arguments passed."; *)
(* GRQProduct[v_?VectorQ, u_?VectorQ] := v.metric.u /; Length[u]==Length[v]==dim; *)
(* GRQProduct[v_?VectorQ, u_?VectorQ] := (Message[GRQProduct::wlen, Length[u], Length[v]]; Abort[];); *)
(* GRQProduct[___] := (Message[GRQProduct::warg]; Abort[];); *)
(* GRQVectorNorm[v_?VectorQ] := GRQProduct[v, v]; *)
(* GRQVectorNormalize::wlen = "Vector of incompativle lenth (`1`) passed."; *)
(* GRQVectorNormalize[v_?VectorQ] := v/Sqrt[Abs@GRQVectorNorm[v]] /; Length[v] ==dim; *)
(* GRQVectorNormalize[v_?VectorQ] := (Message[GRQVectorNormalize::wlen, Length[v]]; Abort[];); *)

GRQ::wind = "Function `1` called with wrong set of indices ind=`2`.";


GRQCreateMetric::warg = "Wrong input arguments g=`1`, c=`2`.";
GRQCreateMetric::nomthd = "There is no method `1` for GRQMetric objects.";

Options[GRQCreateMetric] = {"FirstIndex" -> 0};

GRQCreateMetric[g_?MatrixQ, c_?VectorQ, opts:OptionsPattern[]] := (
   GRQMetric[{g, c, Length[c], GRQSimplify@Det[g], GRQSimplify@Inverse[g]},
	     OptionValue["FirstIndex"]]
   ) /; Length[g] == Length[c] && ListOfSymbolsQ[c] && Not[DuplicatesQ[c]] && SymmetricMatrixQ[g];

GRQCreateMetric[g_?MatrixQ, ginv_?MatrixQ, c_?VectorQ, opts:OptionsPattern[]] := (
   GRQMetric[{g, c, Length[c], GRQSimplify@Det[g], ginv},
	     OptionValue["FirstIndex"]]
   ) /; Length[g] == Length[c] == Length[ginv] && ListOfSymbolsQ[c] && Not[DuplicatesQ[c]] && SymmetricMatrixQ[g] && SymmetricMatrixQ[ginv];

GRQCreateMetric[g_, c_, opts___] := (Message[GRQCreateMetric::warg, g, c]; Abort[];);

GRQMetric[{g_, c_, n_, detg_, ig_}, ___]["Dimension"] := n;
GRQMetric[{g_, c_, n_, detg_, ig_}, ___]["Coordinates"] := c;
GRQMetric[{g_, c_, n_, detg_, ig_}, ___]["MetricMatrix"] := g;
GRQMetric[{g_, c_, n_, detg_, ig_}, ___]["InverseMetricMatrix"] := ig;
GRQMetric[{g_, c_, n_, detg_, ig_}, s_, ___]["FirstIndex"] := s;
GRQMetric[{g_, c_, n_, detg_, ig_}, s_, ___]["Methods"] :=
   {"Dimension", "Coordinates", "MetricMatrix", "InverseMetricMatrix",
   "FirstIndex", "Methods"} // Sort;

GRQMetric[{g_, c_, n_, detg_, ig_}, s_, ___][method_String] := (
   Message[GRQCreateMetric::nomthd, method]; Abort[]
   );

(* GRQMetric[{g_, c_, n_, detg_, ig_}, fi_, ___][{{}, {i_, j_}}] :=
   If[fi == 0, g[[i + 1, j + 1]], g[[i, j]]] /; inRangeQ[i,j, {fi, fi+n-1}]; *)
GRQMetric[{g_, c_, n_, detg_, ig_}, fi_, ___][{{}, {i_, j_}}] :=
   g[[i - fi + 1, j - fi + 1]] /; inRangeQ[i,j, {fi, fi+n-1}];

(* GRQMetric[{g_, c_, n_, detg_, ig_}, fi_, ___][{{i_, j_}, {}}] :=
   If[fi == 0, ig[[i + 1, j + 1]], ig[[i, j]]] /; inRangeQ[i,j, {fi, fi+n-1}]; *)
GRQMetric[{g_, c_, n_, detg_, ig_}, fi_, ___][{{i_, j_}, {}}] :=
   ig[[i - fi + 1, j - fi + 1]] /; inRangeQ[i,j, {fi, fi+n-1}];

GRQMetric[__][arg___] := (Message[GRQ::wind, GRQMetric, {arg}]; Abort[];);

GRQMetric /: Length[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] := Length[g];
GRQMetric /: Dimensions[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] :=
   Dimensions[g];
GRQMetric /: Det[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] := detg;
GRQMetric /: Tr[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] := Tr[g];
GRQMetric /: Diagonal[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] :=
   Diagonal[g];
GRQMetric /: Normal[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] := g;
GRQMetric /: Inverse[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] := ig;
GRQMetric /: MatrixForm[GRQMetric[{g_, c_, n_, detg_, ig_}, ___]] :=
   MatrixForm[g];


MakeBoxes[g_GRQMetric, fmt_] ^:=
   Module[{shown, hidden, icon},
      shown = {
	 {BoxForm`MakeSummaryItem[
	    {"Coordinates: ", g["Coordinates"]}, fmt], SpanFromLeft}
      };
      hidden = {
	 {BoxForm`MakeSummaryItem[
	    {"FirstIndex: ", g["FirstIndex"]}, fmt], SpanFromLeft}

      };
      icon = ArrayPlot[Normal[g], ImageSize -> 25];

      BoxForm`ArrangeSummaryBox[GRQMetric, g, icon, shown, hidden, fmt,
			  "Interpretable" -> False]
   ];


(* GRQChristoffel[g_GRQMetric][{{i_}, {k_, l_}}] := (
   GRQChristoffel[g][{{i}, {k, l}}] =
   GRQSimplify@With[{fi = g["FirstIndex"], dim = g["Dimension"],
		     x = g["Coordinates"]},
	If[fi == 0,
	   1/2 Sum[g[{{i, m}, {}}] (
	      D[g[{{}, {m, k}}], x[[l + 1]]] +
	      D[g[{{}, {m, l}}], x[[k + 1]]] -
	      D[g[{{}, {k, l}}], x[[m + 1]]]), {m, 0, dim - 1}],
	   1/2 Sum[ g[{{i, m}, {}}] (
	      D[g[{{}, {m, k}}], x[[l]]] +
	      D[g[{{}, {m, l}}], x[[k]]] -
	      D[g[{{}, {k, l}}], x[[m]]]), {m, dim}]
	]
   ]
   )/; inRangeQ[i, k, l, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}]; *)

GRQChristoffel[g_GRQMetric][{{i_}, {k_, l_}}] := (
   GRQChristoffel[g][{{i}, {k, l}}] =
   GRQSimplify@Module[{fi = g["FirstIndex"], dim = g["Dimension"], x},
      x = g["Coordinates"][[#-fi+1]]&;
      1/2 Sum[ g[{{i, m}, {}}] (
         D[g[{{}, {m, k}}], x[l]] +
         D[g[{{}, {m, l}}], x[k]] -
         D[g[{{}, {k, l}}], x[m]]), {m, fi, fi + dim - 1}]
   ]
   )/; inRangeQ[i, k, l, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}];

GRQChristoffel[g_][arg___] := (Message[GRQ::wind, GRQChristoffel, {arg}]; Abort[];);


(* GRQRiemann[g_GRQMetric][{{r_}, {s_, m_, n_}}] := (
   GRQRiemann[g][{{r}, {s, m, n}}] =
   GRQSimplify @ With[{fi = g["FirstIndex"], dim = g["Dimension"],
      x = g["Coordinates"],
	 Gamma = Function[{i, j, k}, GRQChristoffel[g][{{i}, {j, k}}]]},
	If[fi == 0,
	   D[Gamma[r, n, s], x[[m + 1]]] - D[Gamma[r, m, s], x[[n + 1]]] +
	   Sum[Gamma[r, m, l]*Gamma[l, n, s], {l, 0, dim - 1}] -
	   Sum[Gamma[r, n, l]*Gamma[l, m, s], {l, 0, dim - 1}]
	 ,
	   D[Gamma[r, n, s], x[[m]]] - D[Gamma[r, m, s], x[[n]]] +
	   Sum[Gamma[r, m, l]*Gamma[l, n, s], {l, dim}] -
	   Sum[Gamma[r, n, l]*Gamma[l, m, s], {l, dim}]
	]
   ]
   )/; inRangeQ[r, s, m, n, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}]; *)

GRQRiemann[g_GRQMetric][{{r_}, {s_, m_, n_}}] := (
   GRQRiemann[g][{{r}, {s, m, n}}] =
   GRQSimplify @ Module[{fi = g["FirstIndex"], dim = g["Dimension"],
      x,
    Gamma = Function[{i, j, k}, GRQChristoffel[g][{{i}, {j, k}}]]},
    x = g["Coordinates"][[#-fi+1]]&;
      D[Gamma[r, n, s], x[m]] - D[Gamma[r, m, s], x[n]] +
      Sum[Gamma[r, m, l]*Gamma[l, n, s], {l, fi, fi + dim - 1}] -
      Sum[Gamma[r, n, l]*Gamma[l, m, s], {l, fi, fi + dim - 1}]
   ]
   )/; inRangeQ[r, s, m, n, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}];

GRQRiemann[g_][arg___] := (Message[GRQ::wind, GRQRiemann, {arg}]; Abort[];);


(* GRQRicci[g_GRQMetric][{{}, {m_, n_}}] := (
   GRQRicci[g][{{}, {m, n}}] =
   GRQSimplify @ With[{fi = g["FirstIndex"], dim = g["Dimension"]},
	If[fi == 0,
	   Sum[GRQRiemann[g][{{l}, {m, l, n}}], {l, 0, dim - 1}],
	   Sum[GRQRiemann[g][{{l}, {m, l, n}}], {l, dim}]
	]
   ]
   )/; inRangeQ[m, n, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}]; *)

GRQRicci[g_GRQMetric][{{}, {m_, n_}}] := (
   GRQRicci[g][{{}, {m, n}}] =
   GRQSimplify @ With[{fi = g["FirstIndex"], dim = g["Dimension"]},
      Sum[GRQRiemann[g][{{l}, {m, l, n}}], {l, fi, fi + dim - 1}]
   ]
   )/; inRangeQ[m, n, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}];

GRQRicci[g_][arg___] := (Message[GRQ::wind, GRQRicci, {arg}]; Abort[];);


(* GRQRicciScalar[g_GRQMetric][] :=
   GRQRicciScalar[g][] =
   GRQSimplify @ With[{fi = g["FirstIndex"], dim = g["Dimension"]},
	If[fi == 0,
	   Sum[g[{{m, n}, {}}] GRQRicci[g][{{}, {m, n}}], {m, 0, dim - 1}, {n, 0, dim - 1}],
	   Sum[g[{{m, n}, {}}] GRQRicci[g][{{}, {m, n}}], {m, dim}, {n, dim}]
	]
   ]; *)

GRQRicciScalar[g_GRQMetric][] :=
   GRQRicciScalar[g][] =
   GRQSimplify @ With[{fi = g["FirstIndex"], dim = g["Dimension"]},
      Sum[g[{{m, n}, {}}] GRQRicci[g][{{}, {m, n}}], {m, fi, fi + dim - 1}, {n, fi, fi + dim - 1}]
   ];


GRQEinstein[g_GRQMetric][{{}, {i_Integer, j_Integer}}] := (
   GRQEinstein[g][{{}, {i, j}}] = GRQSimplify[
      GRQRicci[g][{{}, {i, j}}] - 1/2*g[{{}, {i, j}}]*GRQRicciScalar[g][]]
   ) /; inRangeQ[i, j, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}];

GRQEinstein[g_][arg___] := (Message[GRQ::wind, GRQEinstein, {arg}]; Abort[];);


GRQCoordsToOneForms[c_List, d_String: "d"] :=
  ToExpression[StringJoin[{d, ToString[#]}]] & /@ c /; ListOfSymbolsQ[c] && Not[DuplicatesQ[c]];

GRQCoordsToOneForms[arg___] := (Message[GRQ::wind, GRQCoordsToOneForms, {arg}]; Abort[];);


GRQLineElementToMatrix[ds_, dc_List] :=
  Map[Coefficient[ds, #] &, Outer[Times, dc, dc], {2}]*
   Normal@SparseArray[{{i_, i_} :> 1, {_, _} :> 1/2}, {Length[dc],
      Length[dc]}] /; ListOfSymbolsQ[dc] && Not[DuplicatesQ[dc]];

GRQLineElementToMatrix[arg___] := (Message[GRQ::wind, GRQLineElementToMatrix, {arg}]; Abort[];);


GRQClearMetricCache[g_GRQMetric] :=
   Quiet[
      Module[{fi = g["FirstIndex"], dim = g["Dimension"], iter},
	     iter = If[fi == 0, {fi, dim - 1}, {1, dim}];
	     Do[GRQChristoffel[g][{{al}, {mu, nu}}] = .,
		Evaluate@{al, Sequence @@ iter},
		Evaluate@{mu, Sequence @@ iter},
		Evaluate@{nu, Sequence @@ iter}];

	     Do[GRQRiemann[g][{{ro}, {si, mu, nu}}] = .,
		Evaluate@{ro, Sequence @@ iter},
		Evaluate@{si, Sequence @@ iter},
		Evaluate@{mu, Sequence @@ iter},
		Evaluate@{nu, Sequence @@ iter}
	     ];

	     GRQRicciScalar[g][] = .;

	     Do[GRQRicci[g][{{}, {mu, nu}}] = .,
		Evaluate@{mu, Sequence @@ iter},
		Evaluate@{nu, Sequence @@ iter}
	     ];

	     Do[GRQEinstein[g][{{}, {mu, nu}}] = .,
		Evaluate@{mu, Sequence @@ iter},
		Evaluate@{nu, Sequence @@ iter}
	     ];

      ];, {Unset::norep}];


(* Schouten tensor *)
GRQSchouten[g_][{{}, {m_Integer, n_Integer}}] := (
   GRQSchouten[g][{{}, {m,n}}] = 
      With[{dim = g["Dimension"], Ric = GRQRicci[g], R = GRQRicciScalar[g][]},
         1/(dim-2)*(Ric[{{}, {m,n}}] - R/(2*(dim-1))*g[{{}, {m,n}}])
      ]
  ) /; inRangeQ[m, n, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}];


(* Cotton tensor *)
GRQCotton[g_][{{}, {i_Integer, j_Integer, k_Integer}}] := (
   GRQCotton[g][{{}, {i, j, k}}] = 
      With[{dim = g["Dimension"], Gamma = GRQChristoffel[g], X = Part[g["Coordinates"],#+1]&, L = GRQSchouten[g]},
         D[L[{{},{j,k}}], X[i]] - D[L[{{},{i,k}}], X[j]] +
          Sum[Gamma[{{a},{k,j}}]*L[{{},{i,a}}] - Gamma[{{a},{k,i}}]*L[{{},{j,a}}], {a,0, dim-1}]
      ]
  ) /; inRangeQ[i, j, k, {g["FirstIndex"], g["FirstIndex"]+g["Dimension"]-1}];


(* Weyl tensor (definition from Wald) *)
Clear[GRQWeyl]; 
GRQWeyl[g_][{{aa_Integer}, {bb_Integer, cc_Integer, 
    dd_Integer}}] := (GRQWeyl[g][{{aa}, {bb, cc, dd}}] = 
   Module[{dim = g["Dimension"], Riemann = GRQRiemann[g], 
     Ricci = GRQRicci[g], R = GRQRicciScalar[g][], 
     delta = KroneckerDelta,
     Riccidnup},
    Riccidnup[{{jj_}, {ii_}}] := 
     Sum[Ricci[{{}, {ii, kk}}] g[{{kk, jj}, {}}], {kk, 0, dim - 1}];
    
    Riemann[{{aa}, {bb, cc, dd}}] - 
     1/(dim - 
       2) (delta[aa, cc] Ricci[{{}, {dd, bb}}] - 
        delta[aa, dd] Ricci[{{}, {cc, bb}}] - 
        g[{{}, {bb, cc}}] Riccidnup[{{aa}, {dd}}] + 
        g[{{}, {bb, dd}}] Riccidnup[{{aa}, {cc}}]) + 
     1/((dim - 1) (dim - 2))
       R (delta[aa, cc]*g[{{}, {dd, bb}}] - 
        delta[aa, dd] g[{{}, {cc, bb}}])
    ]);


(* GRQKretschmann[{{}, {}}] := 0; *)


(* Operations *)


(* GRQCovariantD[a_Integer][t_Symbol, {up_List, down_List}] := *)
(* 	D[t[{up, down}], *)


(* The 3+1 form of the space-time metric *)
GRQMetric3p1Form[g_GRQMetric] := GRQMetric3p1Form[Normal[g]];
GRQMetric3p1Form[g_?MatrixQ] :=
  Module[{alpha, betadn, betaup, gamma},
   betadn = g[[1, 2 ;;]];
   gamma = g[[2 ;;, 2 ;;]];
   betaup = Inverse[gamma].betadn;
   alpha = Sqrt[betadn.Inverse[gamma].betadn - g[[1, 1]]];
   {"\[Alpha]" -> alpha,
    "\!\(\*SuperscriptBox[\(\[Beta]\), \(i\)]\)" -> betaup,
    "\!\(\*SubscriptBox[\(\[Gamma]\), \(ij\)]\)" -> gamma}
  ];

GRQExtrinsicCurvature[g_GRQMetric] :=
  Module[{nup = -First@Inverse[g]*(-Inverse[g][[1, 1]])^(-1/2), ndn,
    X, dim},
   ndn = nup.Normal[g];
   X = g["Coordinates"];
   dim = g["Dimension"];
   Table[-D[ndn[[b + 1]], X[[a + 1]]] +
     Sum[GRQChristoffel[g][{{c}, {a, b}}]*ndn[[c + 1]], {c, 0,
       dim - 1}] -
     ndn[[a + 1]]*
      Sum[nup[[
         c + 1]]*(D[ndn[[b + 1]], X[[c + 1]]] -
          Sum[GRQChristoffel[g][{{d}, {c, b}}]*ndn[[d + 1]], {d, 0,
            dim - 1}]), {c, 0, dim - 1}], {a, 0, dim - 1}, {b, 0,
     dim - 1}]
   ];

End[];

EndPackage[];
